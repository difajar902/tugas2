import React from 'react';
import {View, Image, Text, StyleSheet, ScrollView} from 'react-native';
import Thumbnail from './components/Thumbnail';
import Thumbnail2 from './components/Thumbnail2';
import Thumbnail3 from './components/Thumbnail3';
import Thumbnail4 from './components/Thumbnail4';

const App = () => {
    return (
        <View>
            <ScrollView>
                <View style={styles.box}>
                    <Image style={styles.yt} source={require('./components/yt.png')}/>
                    <View>
                        <Text style={styles.text}>YouTube</Text>
                    </View>
                    <View>
                        <Image style={styles.notif} source={require('./components/notif.png')}/>
                    </View>
                    <View>
                        <Image style={styles.vector} source={require('./components/vector.jpg')}/>
                    </View>
                    <View>
                        <Image style={styles.user} source={require('./components/dinifajariyah.png')}/>
                    </View>
                </View>
                <View>
                    <Thumbnail />
                    <Thumbnail2 />
                    <Thumbnail3 />
                    <Thumbnail4 />
                </View>
            </ScrollView>
        </View>
    );
};
const styles = StyleSheet.create({
    text: {
        fontSize: 20,
        fontFamily: 'britannic bold',
        margin: 10,
        marginLeft: 0
    },
    box: {
        backgroundColor: 'white',
        height: 60,
        flexDirection: 'row'
    },
    yt: {
        borderRadius: 80,
        width: 50,
        height: 50,
        marginLeft: 5,
    },
    notif: {
        marginTop: 10,
        borderRadius: 80,
        width: 20,
        height: 30,
        marginLeft: 70,
        marginBottom: 30
    },
    vector: {
        marginTop: 10,
        borderRadius: 80,
        width: 30,
        height: 30,
        marginLeft: 25
    },
    user: {
        marginTop: 10,
        borderRadius: 80,
        width: 30,
        height: 30,
        marginLeft: 25
    }

})

export default App;