import React from 'react';
import {View, Text, Image, StyleSheet, ScrollView} from 'react-native';

const Thumbnail3 = () =>{
    return(
        <View>
            <ScrollView>
                <View>
                    <Image style={setting.logo} source={require('./violin_keyboard.png')}/>
                </View> 
                <View style={setting.container}>
                    <Image style={setting.bulat} source={require('./dinifajariyah.png')} />
                    <View>
                        <Text style={setting.text}>Learn to accompany the songs on the PIANO & VIOLIN part 3 with me</Text>
                        <Text style={{marginBottom: 20, marginLeft: 5}}>Df'dikeys . 75jt x ditonton . 1 minggu yang lalu</Text>
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};
const setting = StyleSheet.create({
    logo: {
        marginBottom: 10,
        height: 200,
    },
    text: {
        textAlign: 'justify',
        fontSize: 15,
        fontFamily: 'arial black',
        fontWeight: 'bold',
        marginLeft: 6
    },
    bulat: {
        borderRadius: 100,
        width: 40,
        height: 40,
        marginLeft: 10
    },
    container: {
        flexDirection: 'row',
    }
})
export default Thumbnail3;